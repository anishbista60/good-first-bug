# Good First Bug 1

 ## [About](https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/blob/main/README.md)

## Content

#### Suggested Projects

The following is a list of popular projects currently listing beginner issues. These are 'suggested' projects and issues that participants can help out with. Participants are not required to restrict themselves to the following issues. 

Many other beginner issues can be found by searching github for issues tagged with "Good First Issue", or by using sites that aggregate beginner issues such as :

- [GoodFirstIssue.com](http://goodfirstissue.com/)
- [GitHub issues sorted by Good First Issue tag](https://github.com/topics/good-first-issue)
- [GoodFirstIssue.dev](https://goodfirstissue.dev/)
- [GoodFirstIssueFinder](https://finder.eddiehub.io/)

| Project | Link to Issues tagged with 'Good First Issue' | Programming Language | Project Details | Volunteers |
|--|--|--|--|--|
|[ vicky002/AlgoWiki ](https://github.com/vicky002/AlgoWiki)| [○](https://github.com/vicky002/AlgoWiki/labels/good%20first%20issue) | HTML |  Repository which contains links and resources on different topics of Computer Science.  | |
|[ kubernetes/website ](https://github.com/kubernetes/website)| [○](https://github.com/kubernetes/website/labels/good%20first%20issue) | HTML |  Kubernetes website and documentation repo| |
|[ shahednasser/awesome-resources ](https://github.com/shahednasser/awesome-resources)| [○](https://github.com/shahednasser/awesome-resources/labels/good%20first%20issue) | Markdown |  List of helpful resources added by the community for the community!  | |
|[ arxiv-vanity/engrafo ](https://github.com/arxiv-vanity/engrafo)| [○](https://github.com/arxiv-vanity/engrafo/labels/good%20first%20issue) | HTML |  Convert LaTeX documents into beautiful responsive web pages using LaTeXML.  | |
|[ collectd/collectd ](https://github.com/collectd/collectd)| [○](https://github.com/collectd/collectd/labels/good%20first%20issue) | C | The system statistics collection daemon. Please send Pull Requests here! | |
|[ obsproject/obs-studio ](https://github.com/obsproject/obs-studio) | [○](https://github.com/obsproject/obs-studio/labels/Good%20first%20issue) | C | OBS Studio - Free and open source software for live streaming and screen recording | |
|[ qmk/qmk_firmware ](https://github.com/qmk/qmk_firmware)| [○](https://github.com/qmk/qmk_firmware/issues?q=is%3Aopen+is%3Aissue+label%3A%22good+first+issue%22) | C | Open-source keyboard firmware for Atmel AVR and Arm USB families | |
|[ matplotlib/matplotlib ](https://github.com/matplotlib/matplotlib)| [○](https://github.com/matplotlib/matplotlib/labels/good%20first%20issue) | Python |  matplotlib| |
|[ ethereum/web3.py ](https://github.com/ethereum/web3.py)| [○](https://github.com/ethereum/web3.py/labels/good%20first%20issue) | Python |  A python interface for interacting with the Ethereum blockchain and ecosystem.  | |
|[ scipy/scipy ](https://github.com/scipy/scipy)| [○](https://github.com/scipy/scipy/labels/good%20first%20issue) | Python |  SciPy library main repository  | |
|[ bokeh/bokeh ](https://github.com/bokeh/bokeh)| [○](https://github.com/bokeh/bokeh/labels/good%20first%20issue) | Python |  Interactive Data Visualization in the browser, from  Python  |Gunashree |
|[ pyqtgraph/pyqtgraph ](https://github.com/pyqtgraph/pyqtgraph)| [○](https://github.com/pyqtgraph/pyqtgraph/labels/good%20first%20issue) | Python |  Fast data visualization and GUI tools for scientific / engineering applications  | |
|[ nukeop/nuclear ](https://github.com/nukeop/nuclear)| [○](https://github.com/nukeop/nuclear/labels/good%20first%20issue) | TypeScript |  Streaming music player that finds free music for you  | |
|[ Chocobozzz/PeerTube ](https://github.com/Chocobozzz/PeerTube)| [○](https://github.com/Chocobozzz/PeerTube/labels/good%20first%20issue) | TypeScript |  ActivityPub-federated video streaming platform using P2P directly in your web browse  | |
|[ elastic/kibana ](https://github.com/elastic/kibana)| [○](https://github.com/elastic/kibana/labels/good%20first%20issue) | TypeScript | Your window into the Elastic Stack | |
|[ sitespeedio/sitespeed.io ](https://github.com/sitespeedio/sitespeed.io)| [○](https://github.com/sitespeedio/sitespeed.io/labels/good%20first%20issue) | JavaScript |  Sitespeed.io is an open source tool that helps you monitor, analyze and optimize your website speed and performance, based on performance best practices advices from the coach and collecting browser metrics using the Navigation Timing API, User Timings and Visual Metrics (FirstVisualChange, SpeedIndex & LastVisualChange).  | |
|[ shravan20/github-readme-quotes ](https://github.com/shravan20/github-readme-quotes)| [○](https://github.com/shravan20/github-readme-quotes/labels/good%20first%20issue) | JavaScript |  Dynamic quote generator for your GitHub readmes - Give a poetic touch to readmes   | |
|[ vercel/next.js ](https://github.com/vercel/next.js)| [○](https://github.com/vercel/next.js/labels/good%20first%20issue) | JavaScript |  The React Framework    | |
|[ ccextractor/taskwarrior-flutter ](https://github.com/CCExtractor/taskwarrior-flutter)| [○](https://github.com/CCExtractor/taskwarrior-flutter/issues) | Flutter |  This project aims to build an app for Taskwarrior. It is your task management app across all platforms. It helps you manage your tasks and filter them as per your needs.    | Rijuth |
|[ odoo/odoo ](https://github.com/odoo/odoo)|[○](https://github.com/odoo/odoo/issues) | JavaScript/Python/HTML/CSS | Odoo is a suite of web based open source business apps. | Rithwik |

#### Sample Projects

- [First PR](https://github.com/fsmk/git-sample-project-2/)



- [Sample Project 0](https://github.com/fsmk/git-sample-project-0/)
- [Sample Project 1](https://github.com/fsmk/git-sample-project-1/)
- [Sample Project 2](https://github.com/fsmk/git-sample-project-2/)

#### Guides

##### Git
- [First Contribution Guide](https://github.com/firstcontributions/first-contributions)
- [MIT missing semester - Version Control](https://missing.csail.mit.edu/2020/version-control/)
- [What is Git ?](https://www.git-scm.com/book/en/v2/)
- [Git & GitHub Crash Course For Beginners](https://www.youtube.com/watch?v=SWYqp7iY_Tc)
- [Git Cheat Sheet](https://cs.fyi/guide/git-cheatsheet)

## Date & Time (Tentative)
June 24th 2023 ; 10:00 AM - 2:00 PM

## Venue
CMRIT College

## Human Resource

- Event Coordinator : Shijil/Rameez
- Technical Specialist : Gautham
- Volunteers : Rithwik, Kevin, Gunashree, Pallavi
- PR Coordinator :
- Logistics Manager :

## Program structure
| Duration | Activity | Remarks |
|--|--|--|
| 10:00 AM - 10:15 AM  | FSMK Induction  ||
| 10:15 AM - 10:30 AM | Team Building Activity / Ice Breaking Event & Group Formation ||
| 10:30 AM - 11:30 AM| Walk through of where/how to start contribute ?  * Demo on fixing a bug * Raising a PR * Reporting Issue * Present sources where   ||
| 11:30 AM - 12:00 AM | Present Problem & T Break||
| 12:00 AM - 01:30 PM      | Hackathon||
| 01:30 PM - 02:00 PM      | Share the result & Experirnce + Photo Session||

## Post Event
- Collect the contact details
- Encourage participants to join commune.fsmk.org
- Further discussion to happen on commune.fsmk.org - Discuss and Forum
